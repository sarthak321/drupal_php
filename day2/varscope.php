<?php
//local scope
function localScope() {
$x =99;
echo "local variable".$x;
echo "<br>";
}
localScope();
//global scope
$y=99;
function globalScope(){
  global $y;
  $y=78;
  echo "global variable".$y;
  echo "<br>";
}
globalScope();
echo $y;
echo "<br>";

function staticScope()
{
  static $z=0;
  $z++;
  echo $z;
  echo "<br>";
} 
staticScope();
staticScope();
?>
