<?php
function foo() {
    echo "In foo()<br />\n";
}

function bar($arg = 'sarthak')
{
    echo "In bar(); argument was '$arg'.<br />\n";
}

function echoit($string)
{
    echo $string;
}

$func = 'foo';
$func();        // This calls foo()

$func = 'bar';
$func();  // This calls bar()

$func = 'echoit';
$func('test');  // This calls echoit()
?>

